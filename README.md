# Searching algorithms vizualization

Searching algorithms vizualization made with Vue 3! Enjoy.
[Vizualization's demo, powered by Gitlab's pages](https://sebascoronado.gitlab.io/searching-algorithms-vizualization/)
---

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
