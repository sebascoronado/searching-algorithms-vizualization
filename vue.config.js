module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/' + 'searching-algorithms-vizualization' + '/'
      : '/'
  }