/* eslint-disable no-unused-vars */

class QElement { 
    constructor(element, priority) 
    { 
        this.element = element; 
        this.priority = priority; 
    } 
} 

function PQueue() {

    this.init = function () {
        this.data = []
    };

    this.push = function (element, priority) {
        var qElement = new QElement(element, priority); 
        var contain = false;
        
        for (var i = 0; i < this.data.length; i++) { 
            if (this.data[i].priority > qElement.priority) { 
                this.data.splice(i, 0, qElement); 
                contain = true; 
                break; 
            } 
        } 
      
        if (!contain) { 
            this.data.push(qElement); 
        } 
    };

    this.isEmpty = function () {
        return this.data.length == 0;
    };

    this.pop = function () {
        return this.data.shift(); 
    };

    this.front = function () {
        return this.data[0]
    };

    this.clear = function () {
        this.data = [];
    };

    this.init();
}
/* eslint-enable no-unused-vars */
export default PQueue;
