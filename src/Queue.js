/* eslint-disable no-unused-vars */
function Queue() {
    this.init = function () {
        this.data = []
    };

    this.push = function (element) {
        this.data.push(element)
    };

    this.isEmpty = function () {
        return this.data.length == 0;
    };

    this.pop = function () {
        this.data.shift();
    };

    this.front = function () {
        return this.data[0]
    };

    this.clear = function () {
        this.data = [];
    };

    this.init();
}
/* eslint-enable no-unused-vars */
export default Queue;
